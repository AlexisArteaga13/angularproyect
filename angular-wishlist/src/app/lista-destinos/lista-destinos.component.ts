import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import {AppState} from './../app.module';
import { ElegidoFavoritoAction } from '../models/destino-viajes-state.model';
@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded:EventEmitter<DestinoViaje>;
  updates:string[];
  constructor(private destinosApiClient:DestinosApiClient,private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.updates=[];
    this.store.select(state => state.destinos.favorito)
    .subscribe(data => {
      const f = data;
      if (f != null) {
        this.updates.push('Se eligió: ' + f.nombre);
      }
    });
  }

  ngOnInit(): void {
  }
  agregado(d:DestinoViaje):boolean {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);

   /*this.destinos.push(new DestinoViaje(nombre,url)); 
   console.log(this.destinos);
    console.log(new DestinoViaje(nombre,url))
    console.log(nombre);
    console.log(url);*/
    return false;
  }
  elegido(e: DestinoViaje){
    
    /*this.destinosApiClient.getAll().forEach(function (x){
      x.setSelected(false); 
      console.log(x);
    });
    e.setSelected(true);*/
    this.destinosApiClient.elegir(e);
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }
  getAll(){
    
  }
}
